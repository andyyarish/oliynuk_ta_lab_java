package areaRectangle;

import areaRectangle.models.Calculator;
import areaRectangle.areaCustomizedExceptions.NegativeValueException;

public class AreaMainClass {

    public static void main(String[] args) {

        Calculator calculator = new Calculator();

        // Exception is caught on the parsing - program goes on
        try {
            calculator.calculateWithException("asd", "9");
        } catch (NegativeValueException e) {
            System.out.println(e.getMessage());
        }

        // Exception is caught on the verification if numbers are negative - program goes on
        try {
            calculator.calculateWithException("3", "-9");
        } catch (NegativeValueException e) {
            System.out.println(e.getMessage());
        }

        // Exception is not caught because it extends RuntimeException
        calculator.calculateWithRuntimeException(3, -9);
    }
}
