package arrays.game.services;

import arrays.game.models.Door;
import arrays.game.models.Hero;
import logers.BaseLoggerClass;
import static arrays.arraysmethods.Utils.printArray;
import static arrays.arraysmethods.Utils.addElement;

public class Service extends BaseLoggerClass {

    private Hero hero;
    private Door[] doors;

    public Service() {

        hero = new Hero();
        generateLog(Service.class.toString());
    }

    public void introduceHero() {
        logger.info(hero);
    }

    public void getDoors(int numberOfDoors) {
        doors = new Door[numberOfDoors];
        for (int i = 0; i < doors.length; i++) {
            doors[i] = new Door();
        }
        for (int i = 0; i < doors.length; i++) {
            logger.debug("Door " + (i + 1) + ": ");
            if (doors[i].isMonster) {
                logger.warn(doors[i].monster);
            } else {
                logger.info(doors[i].artifact);
            }
        }
    }

    public void demise() {
        logger.debug("-------------------------------------");
        logger.error("Death jeopardy behind these doors: ");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i].isMonster && (doors[i].monster.power > 25)) {
                logger.error(i + 1 + "   ");
            }
        }
    }

    public void victory() {
        logger.debug("-------------------------------------");
        logger.warn("Victory and Artifact behind these doors: ");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i].isMonster && (doors[i].monster.power <= 25)) {
                logger.info(i + 1 + "   ");
            }
        }
    }

    public void safeOrderOfDoors(){
        int [] sateOrderArr = new int[0];

        for (int i = 0; i < doors.length; i++) {
            if (!doors[i].isMonster||
                    (doors[i].isMonster && (doors[i].monster.power <= 25))) {
                sateOrderArr = addElement(sateOrderArr, i+1);
            }
        }
        logger.debug("-------------------------------------------------------");
        logger.warn("Safe order of doors to open and stay alive : "+
                printArray(sateOrderArr));
    }

}
