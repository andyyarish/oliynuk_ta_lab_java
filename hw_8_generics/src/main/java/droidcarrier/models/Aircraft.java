package droidcarrier.models;

public class Aircraft {

     String name;

    public Aircraft(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "name=" + name +
                '}';
    }
}
