package stringsapp;

import logers.BaseLoggerClass;
import stringsapp.ui.MyView;

public class MainClassStringsApp extends BaseLoggerClass {

    public static void main(String[] args) {
        new MyView().display();
    }
}
