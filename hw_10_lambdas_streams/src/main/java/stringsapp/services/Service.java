package stringsapp.services;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Service {

    public int calculateNumberOfUniqueWords(List<String> list) {
        return list.stream()
                // to cut nonUnique words
                .distinct()
                .collect(Collectors.toList()).size();
    }

    public List<String> sortUniqueWords(List<String> list) {
        return list.stream()
                .distinct()
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
    }

    public Map<String, Long> calculateNumberOfWords(List<String> list) {
        return list.stream()
                .collect(Collectors.groupingBy(word -> word, Collectors.counting()));
    }

    public Map<Character, Long> calculateNumberOfLowerCaseSymbols(List<String> list) {
        String s = String.join("", list);
        List<Character> chars = s.chars().mapToObj(e -> (char)e)
                .collect(Collectors.toList());
        List<Character> charsLowerCaseOnly = chars.stream().
                filter(Character::isLowerCase).collect(Collectors.toList());
        return charsLowerCaseOnly.stream()
                .collect(Collectors.groupingBy(symbol -> symbol, Collectors.counting()));
    }
}
