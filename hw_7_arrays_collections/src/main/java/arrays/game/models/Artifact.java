package arrays.game.models;

public class Artifact extends RandomGenerator {

    private static final int MIN_POWER = 10;
    private static final int MAX_POWER = 80;

    private int power;

    Artifact() {
        power = rand.nextInt((MAX_POWER - MIN_POWER) + 1) + MIN_POWER;
    }

    @Override
    public String toString() {
        return "Artifact{" +
                "power=" + power +
                "} ";
    }
}
