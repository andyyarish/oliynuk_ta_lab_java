package dao;

import models.Airline;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AirlineDAO implements AbstractDAO {


    @Override
    @SuppressWarnings("unchecked")
    public void saveAll(List<?> airlines) throws Exception {

        JSONArray ja = new JSONArray();
        airlines.forEach(a-> ja.add(gson.toJson(a)));
        FileWriter file = new FileWriter(PATH_AIRLINES_FILE);
        file.write(ja.toJSONString());
        file.flush();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> getAll() throws IOException, ParseException {

        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader(PATH_AIRLINES_FILE);
        JSONArray ja = (JSONArray) jsonParser.parse(reader);
        List<Airline> airlines = new ArrayList<>();
        ja.forEach(jsonObject-> airlines.add(gson.fromJson(jsonObject.toString(), Airline.class)));

        return airlines;
    }
}
