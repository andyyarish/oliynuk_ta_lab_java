package com.example.hw_4_upcoming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hw4UpcomingApplication {

	public static void main(String[] args) {
		SpringApplication.run(Hw4UpcomingApplication.class, args);
	}

}
