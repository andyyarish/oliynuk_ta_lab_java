package collections.deque;

import logers.BaseLoggerClass;

public class MainClassDeque extends BaseLoggerClass {

    public static void main(String[] args) {

        generateLog(MainClassDeque.class.toString());

        logger.info("----------Create CustomizedDeque to test---------");
        CustomizedDeque deque = new CustomizedDeque();

        deque.addFirst("ABC");
        deque.addFirst(111);
        deque.addFirst("DEF");
        deque.addFirst(333);
        deque.addFirst("GHI");
        deque.addFirst(555);

        logger.info("print deque: "+deque.toString());

        logger.info("Test getFirst");
        logger.info(deque.getFirst());
        logger.info("Test getLast");
        logger.info(deque.getLast());

        logger.info("Add last element");
        deque.addLast("LastElement");
        logger.info("Test addLast, getLast");
        logger.info(deque.getLast());
        logger.info("print deque: "+deque.toString());

        logger.info("Test removeFirst, peekFirst");
        logger.info(deque.removeFirst());
        logger.info(deque.peekFirst());
        logger.info("print deque: "+deque.toString());

        logger.info("Test offerFirst, offerLast");
        logger.info(deque.offerFirst("First Element"));
        logger.info(deque.offerLast("Last Element"));
        logger.info("print deque: "+deque.toString());

        logger.info("----------Create empty CustomizedDeque to test---------");
        CustomizedDeque deque2 = new CustomizedDeque();

        logger.info("Test peekFirst, peekLast");
        logger.info(deque2.peekFirst());
        logger.info(deque2.peekLast());

        logger.info("Test pollFirst, pollLast");
        logger.info(deque2.pollFirst());
        logger.info(deque2.pollLast());

        logger.info("Test getLast, getFirst WITH ERROR ");
        try{
            logger.info(deque2.getFirst());
        }catch (ArrayIndexOutOfBoundsException e){
            //e.printStackTrace();
            logger.error("Element does not exist!");
        }
        try {
            logger.info(deque2.getLast());
        }catch (ArrayIndexOutOfBoundsException e){
            //e.printStackTrace();
            logger.error("Element does not exist!");
        }

    }
}
