package services;

import models.Airline;
import models.abstractmodels.Aircraft;

import java.util.List;

public class AirlineService extends AbstarctService{

    @SuppressWarnings("unchecked")
    public String saveAirline(Airline airline) throws Exception {

        List<Airline> airlines = (List<Airline>) airlineDAO.getAll();

        String response ="";

        if(airlines.stream().anyMatch(a -> a.getName().equals(airline.getName()))){
            response= "Airline with this name already exists";
        }else {
            airlines.add(airline);
            airlineDAO.saveAll(airlines);
            response = "Airline saved";
        }

        return response;
    }

    @SuppressWarnings("unchecked")
    public String deleteAirline(Airline airline) throws Exception {

        List<Airline> airlines = (List<Airline>) airlineDAO.getAll();

        String response ="";

        if(airlines.stream().noneMatch(a -> a.getName().equals(airline.getName()))){
            response= "Airline with this name does not exist";
        }else {
            airlines.remove(airline);
            airlineDAO.saveAll(airlines);
            List<Aircraft> aircrafts = (List<Aircraft>) aircraftDAO.getAll();
            aircrafts.removeIf(aircraft -> aircraft.getAirline().equals(airline));
            aircraftDAO.saveAll(aircrafts);
            response = "Airline deleted";
        }

        return response;

    }


}
