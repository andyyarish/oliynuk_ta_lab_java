package com.example.hw_4_upcoming.models;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Entity(name = "Contacts")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString(exclude = {"restaurant", "client"})
@FieldDefaults(level = AccessLevel.PRIVATE)

public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String contactName;
    String email;
    String avatar;

    public Contact(String contactName, String email) {
        this.contactName = contactName;
        this.email = email;
    }

    @ManyToOne(cascade = CascadeType.DETACH,
            fetch = FetchType.LAZY)
    Restaurant restaurant;
    @ManyToOne(cascade = CascadeType.DETACH,
            fetch = FetchType.LAZY)
    Client client;
}
