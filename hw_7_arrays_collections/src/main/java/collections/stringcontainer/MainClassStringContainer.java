package collections.stringcontainer;

import logers.BaseLoggerClass;

import com.google.common.base.Stopwatch;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainClassStringContainer extends BaseLoggerClass {

    public static void main(String[] args) {

        generateLog(MainClassStringContainer.class.toString());

        logger.info("Create a container and fill it with String items");
        StringContainer stringContainer = new StringContainer();
        logger.info("add ABC: "+stringContainer.add("ABC"));
        logger.info("add DEF: "+stringContainer.add("DEF"));
        logger.info("add GHI: "+stringContainer.add("GHI"));
        logger.info("add JKL: "+stringContainer.add("JKL"));
        logger.info("add MNO: "+stringContainer.add("MNO"));
        logger.info("add PQR: "+stringContainer.add("PQR"));
        logger.info("add STU: "+stringContainer.add("STU"));
        logger.info("add VWX: "+stringContainer.add("VWX"));
        logger.info("size: "+stringContainer.size());
        logger.trace("----------------------------------------------------");
        logger.info("contains JKL: "+stringContainer.contains("JKL"));
        logger.info("contains ZZZ: "+stringContainer.contains("ZZZ"));

        logger.trace("----------------------------------------------------");
        logger.info("remove ABC: "+stringContainer.remove("ABC"));
        logger.info("remove STU: "+stringContainer.remove("STU"));
        logger.info("contains STU: "+stringContainer.contains("STU"));
        logger.info("size: "+stringContainer.size());

        logger.trace("----------------------------------------------------");
        logger.info("Fill StringContainer with 10000 items");
        final Stopwatch stopwatch1 = Stopwatch.createStarted();
        StringContainer container = new StringContainer();
        for (int i = 0; i < 10000; i++) {
            container.add(Integer.toString(i));
        }
        stopwatch1.stop();
        logger.warn("StringContainer elapsed time in Milliseconds ==> "
                + stopwatch1.elapsed(TimeUnit.MILLISECONDS));

        logger.trace("----------------------------------------------------");
        logger.info("Fill ArrayList with 10000 items");
        final Stopwatch stopwatch2 = Stopwatch.createStarted();
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            list.add(Integer.toString(i));
        }
        stopwatch2.stop();
        logger.warn("ArrayList elapsed time in Milliseconds ==> "
                + stopwatch2.elapsed(TimeUnit.MILLISECONDS));
    }
}
