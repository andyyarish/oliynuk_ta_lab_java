package patterncommand;

import patterncommand.ui.MyView;

public class MainClassCommand {

    public static void main(String[] args) {
        new MyView().show();
    }
}
