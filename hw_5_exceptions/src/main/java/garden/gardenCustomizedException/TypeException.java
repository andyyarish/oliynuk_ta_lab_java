package garden.gardenCustomizedException;

public class TypeException extends Exception{
    public TypeException( Throwable cause) {
        super("Wrong type input", cause);
    }
}
