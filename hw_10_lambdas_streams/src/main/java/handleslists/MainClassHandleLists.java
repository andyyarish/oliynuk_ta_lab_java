package handleslists;

import logers.BaseLoggerClass;

import java.util.List;

import static handleslists.ListGenerator.*;
import static handleslists.Utils.*;

public class MainClassHandleLists extends BaseLoggerClass {

    public static void main(String[] args) {

        generateLog(MainClassHandleLists.class.toString());
        logger.info("Create lists using different methods ");
        List<Integer> listWithStep = getListWithStep(40, 3, 10);
        logger.info("Print list created with Stream iterations: "+ listWithStep.toString());
        logger.trace("-----------------------------");
        List<Integer> listWithStreamOf = getListStreamOf(10, 12, 120);
        logger.info("Print list created with Stream.of: "+listWithStreamOf.toString());
        logger.trace("-----------------------------");
        List<Integer> listWithForEach = getListForEach(10, 12, 120);
        logger.info("Print list created with ForEach: "+listWithForEach.toString());
        logger.trace("-----------------------------");
        List<Integer> listWithCollect = getListCollect(10, 12, 120);
        logger.info("Print list created with Collect: "+listWithCollect.toString());
        logger.trace("-----------------------------");
        logger.info("Check methods on listWithStreamOf");
        logger.warn("Sum with the use of reduce(): "+calculateSumReduceApproach(listWithStreamOf));
        logger.warn("Sum with the use of Map: "+calculateSumMapApproach(listWithStreamOf));
        logger.warn("Min value: "+calculateMin(listWithStreamOf));
        logger.warn("Max value: "+calculateMax(listWithStreamOf));
        logger.warn("Max value with the use of Comparator: "+calculateMaxComparator(listWithStreamOf));
        logger.warn("Average with plain approach: "+calculateAveragePlainApproach(listWithStreamOf));
        logger.warn("Average with stream approach: "+calculateAverageStreamApproach(listWithStreamOf));
        logger.warn("Number of elements that are bigger than average: "+calculateBiggerThanAverage(listWithStreamOf));
        logger.debug("Statistics of the listWithStreamOf: "+calculateStatistics(listWithStreamOf).toString());
    }
}
