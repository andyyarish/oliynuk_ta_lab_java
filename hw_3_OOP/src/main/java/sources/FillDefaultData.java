package sources;

import dao.AircraftDAO;
import dao.AirlineDAO;
import models.Airline;
import models.abstractmodels.Aircraft;

import java.util.List;

public class FillDefaultData {

    private FillDefaultLists fillSource = new FillDefaultLists();
    private AirlineDAO airlineDAO = new AirlineDAO();
    private AircraftDAO aircraftDAO = new AircraftDAO();

        public void populateDB() throws Exception {
            List<Airline> defAirlines = fillSource.getDefaultAirlines();
            airlineDAO.saveAll(defAirlines);

            List<Aircraft> defAircrafts = fillSource.fill();
            aircraftDAO.saveAll(defAircrafts);
        }

}
