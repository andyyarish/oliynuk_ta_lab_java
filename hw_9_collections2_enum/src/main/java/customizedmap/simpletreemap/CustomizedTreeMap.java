package customizedmap.simpletreemap;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class CustomizedTreeMap<K extends Comparable, V>{

    private Set<K> keySet;
    private List<V> valueList;

    public CustomizedTreeMap() {
        keySet = new TreeSet<>();
        valueList = new ArrayList<>();
    }

    public int size(){
        return keySet.size();
    }

    public boolean isEmpty(){
        return keySet.size() == 0;
    }

    public void clear(){
        keySet = new TreeSet<>();
        valueList = new ArrayList<>();
    }

    public V put(K key, V value){
        keySet.add(key);
        valueList.add(getIndex(key), value);
        return value;
    }

    public V get(K key){
        int position = getIndex(key);
        if(position<0){
            return null;
        }else {
            return valueList.get(position);
        }
    }

    public V remove(K key){
        int position = getIndex(key);
        if(position<0){
            return null;
        }else {
            V value= get(key);
            valueList.remove(value);
            keySet.remove(key);
            return value;
        }
    }

    public String print(){
        StringBuilder s = new StringBuilder();
        for (K k : keySet) {
            s.append("{").append(k.toString()).append(", ")
                    .append(get(k).toString()).append(" }");
        }
        return s.toString();
    }

    private int getIndex(K key){
        int position=0;
        for (K k : keySet) {
            if (k.equals(key)){
                break;
            }
            position++;
        }
        if(position>=keySet.size()){
            position=-1;
        }
        return position;
    }


}
