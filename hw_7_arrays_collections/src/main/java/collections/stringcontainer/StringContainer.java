package collections.stringcontainer;

import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

public class StringContainer {

    private Object[] array;

    StringContainer() {
        array = new String[0];
    }

    boolean add(Object o) {

        boolean flag = true;
        
            if(!(o instanceof String)){
                flag= false;
            }
            else if (array.length == 0) {
                array = new String[1];
                array[0] = o;
            } else {
                String[] arrExtended = new String[array.length + 1];
                arrExtended[0] = (String) o;
                for (int i = 1; i < arrExtended.length; i++) {
                    arrExtended[i] = (String) array[i - 1];
                }
                array = arrExtended;
                flag= true;
            }
        return flag;
    }

    boolean contains(Object o){

        boolean flag = true;

        if(!(o instanceof String)){
            return false;
        }else {
            for (int i = 0; i < array.length; i++) {
                if(array[i].equals(o)){
                    return true;
                }else {
                    flag= false;
                }
            }
        }
        return flag;
    }

    int size(){
        return array.length;
    }

    boolean remove(Object o) {

        boolean flag = true;

        if(!(o instanceof String)){
            flag= false;
        }else if (contains(o)){
            for (int i = 0; i < array.length; i++) {
                if (array[i].equals(o)) {
                    array = ArrayUtils.removeElement(array, array[i]);
                    flag= true;
                }
            }
        }else {
            flag= false;
        }
        return flag;
    }

    @Override
    public java.lang.String toString() {

        return Arrays.toString(array);
    }

}