package functionalinterface;

@FunctionalInterface
public interface IntegerHandelable {

    public int doArithmetic(int a, int b, int c);
}
