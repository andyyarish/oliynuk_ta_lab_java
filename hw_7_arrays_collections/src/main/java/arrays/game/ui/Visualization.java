package arrays.game.ui;

import arrays.game.services.Service;

public class Visualization {

    private Service service;

    public Visualization() {
        this.service = new Service();
    }

    public void runDemo() {

        service.introduceHero();
        service.getDoors(20); // 10 by task
        service.demise();
        service.victory();
        service.safeOrderOfDoors();
    }
}
