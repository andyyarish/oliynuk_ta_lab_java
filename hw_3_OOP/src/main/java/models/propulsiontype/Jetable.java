package models.propulsiontype;

public interface Jetable extends InternalCombustable {

    String TYPE = "Jet Engine";
    int TANK_VOLUME= 5000;

}
