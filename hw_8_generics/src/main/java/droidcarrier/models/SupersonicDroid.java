package droidcarrier.models;

public class SupersonicDroid extends Droid {

    public SupersonicDroid(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "SupersonicDroid {" + name+
                "} ";
    }
}
