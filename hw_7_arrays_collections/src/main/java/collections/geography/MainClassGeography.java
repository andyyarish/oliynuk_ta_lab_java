package collections.geography;

import logers.BaseLoggerClass;

import java.util.*;

public class MainClassGeography extends BaseLoggerClass {

    public static void main(String[] args) {

        generateLog(MainClassGeography.class.toString());

        logger.info("Fill an array of countries");
        Country[] countries = new Country[]{
                new Country("Ukraine", "Kiev"),
                new Country("Poland", "Warsaw"),
                new Country("Belgium", "Brussels"),
                new Country("Denmark", "Copenhagen"),
                new Country("China", "Beijing"),
                new Country("Turkey", "Ankara"),
                new Country("USA", "Washington"),
                new Country("Australia", "Canberra"),
                new Country("Lithuania", "Vilnius"),
                new Country("Latvia", "Riga")
        };

        logger.info("Create 2 lists of objects");
        List<CompareByCountry> cbCoutryName = new ArrayList<>();
        List<CompareByCapital> cbCapital = new ArrayList<>();

        logger.info("Fill lists by countries");
        for (int i = 0; i < countries.length; i++) {
            cbCoutryName.add(new CompareByCountry(countries[i]));
            cbCapital.add(new CompareByCapital(countries[i]));
        }

        logger.info("Sort list by country name");
        cbCoutryName.sort(CompareByCountry::compareTo);
        logger.info("Sort list by capital name");
        cbCapital.sort(CompareByCapital::compareTo);
        logger.warn("----Print sorted by country name----");
        cbCoutryName.forEach(compareByCountry -> logger.info(compareByCountry));
        logger.warn("---Print sorted by capital name---");
        cbCapital.forEach(compareByCapital -> logger.info(compareByCapital));

        logger.info("Binary search on sorted List<CompareByCountry>");
        logger.warn("{China: Some City} index = " + Collections.binarySearch(cbCoutryName,
                new CompareByCountry(new Country("China", "Some City"))));
        logger.info("Binary search on sorted List<CompareByCapital>");
        logger.warn("{Some Country: Riga} index = " + Collections.binarySearch(cbCapital,
                new CompareByCapital(new Country("Some Country", "Riga"))));



    }

}
