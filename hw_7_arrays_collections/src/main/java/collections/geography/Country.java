package collections.geography;

public class Country {

    private String countryName;
    private String capital;

    Country(String country, String capital) {
        this.countryName = country;
        this.capital = capital;
    }

    String getCountryName() {
        return countryName;
    }

    String getCapital() {
        return capital;
    }

    @Override
    public String toString() {
        return "Country{" +
                "countryName='" + countryName + '\'' +
                ", capital='" + capital + '\'' +
                '}';
    }
}
