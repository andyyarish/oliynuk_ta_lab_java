package trafficlightsmenu;

import java.util.LinkedHashMap;
import java.util.Map;

class View {

    private Map<Integer, String> menu;
    private Map<Integer, Showable> methodsMenu;
    private Reader reader = new Reader();

    View() {
        menu = new LinkedHashMap<>();
        menu.put(1, "1 - RED");
        menu.put(2, "2 - YELLOW");
        menu.put(3, "3 - GREEN");
        menu.put(4, "4 - Switch off");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put(1, this::pressButton1);
        methodsMenu.put(2, this::pressButton2);
        methodsMenu.put(3, this::pressButton3);
    }

    private void pressButton1() {
        reader.replyR(TrafficLights.RED.getMessage());
    }

    private void pressButton2() {
        reader.replyY(TrafficLights.YELLOW.getMessage());
    }

    private void pressButton3() {
        reader.replyG(TrafficLights.GREEN.getMessage());
    }

    private String outputMenu() {
        StringBuilder s = new StringBuilder("Enter the option: \n ");
        for (String str : menu.values()) {
           s.append(str).append("\n");
        }
        return s.toString();
    }

    void show() {
        reader.request();
        if(reader.answer==0){
            int key = 0;
            do {
                reader.showDialog(outputMenu());
                try {
                    if ( reader.option == null || reader.option.equals("4")){
                        break;
                    }else {
                        key = Integer.parseInt(reader.option);
                        methodsMenu.get(key).display();
                    }
                } catch (Exception e) {
                    reader.replyW();
                }
            } while (key!=4);
        }else {
            System.exit(0);
        }

    }
}

