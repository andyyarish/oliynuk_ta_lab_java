package arrays.game.models;

public class Hero {

    private int power;

    public Hero() {
        power = 25;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "power=" + power +
                '}';
    }
}
