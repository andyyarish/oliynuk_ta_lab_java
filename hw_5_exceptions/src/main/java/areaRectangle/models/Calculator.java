package areaRectangle.models;

import areaRectangle.areaCustomizedExceptions.NegativeValueException;
import areaRectangle.areaCustomizedExceptions.NegativeValueRuntimeException;

public class Calculator {

    public void calculateWithException(String a, String b) throws NegativeValueException{

        int width = 0;
        int length = 0;

        try {
            width = Integer.parseInt(a);
        }catch (NumberFormatException e){
            System.out.println("Wrong input - first argument - NUMBERS ONLY!");
        }

        try {
            length = Integer.parseInt(b);
        }catch (NumberFormatException e){
            System.out.println("Wrong input - second argument - NUMBERS ONLY!");
        }


        if (width<0){
            throw new NegativeValueException("Negative input - first argument!");
        }else if (length<0){
            throw new NegativeValueException("Negative input - second argument!");
        }else {
            System.out.println("The area of a rectangle = "+ width*length);
        }
    }

    public void calculateWithRuntimeException(int a, int b) {
        if (a<0){
            throw new NegativeValueRuntimeException("Negative input - first argument!");
        }else if (b<0){
            throw new NegativeValueRuntimeException("Negative input - second argument!");
        }else {
            System.out.println("The area of a rectangle = "+ a*b);
        }
    }


}
