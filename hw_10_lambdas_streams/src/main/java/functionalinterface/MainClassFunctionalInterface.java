package functionalinterface;

import logers.BaseLoggerClass;

public class MainClassFunctionalInterface extends BaseLoggerClass {

    public static void main(String[] args) {

        generateLog(MainClassFunctionalInterface.class.toString());

        logger.info("Create interface based realization with simple addition");
        IntegerHandelable interfaceInstance = new IntegerHandelable() {
            @Override
            public int doArithmetic(int a, int b, int c) {
                return a+b+c;
            }
        };
        logger.warn("Invoke method: "+ interfaceInstance.doArithmetic(1, 2, 3));
        logger.trace("-------------------------------------------");
        logger.info("Create lambda based realization to calculate max value");
        IntegerHandelable maxValueInstance = (a, b, c)->{
            if ((a > b) && (a > c)) {
                return a;
            } else if ((b > a) && (b > c)) {
                return b;
            } else {
                return c;
            }
        };
        logger.warn("Invoke method: "+ maxValueInstance.doArithmetic(33, 11, 55));
        logger.trace("-------------------------------------------");
        logger.info("Create lambda based realization to calculate average value");
        IntegerHandelable averageValueInstance = (a, b, c)-> (a + b + c)/3;
        logger.warn("Invoke method: "+ averageValueInstance.doArithmetic(5, 22, 9));
    }
}
