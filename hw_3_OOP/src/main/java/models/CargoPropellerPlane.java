package models;

import models.abstractmodels.Cargo;
import models.propulsiontype.Propellerable;

public class CargoPropellerPlane extends Cargo implements Propellerable {

    public CargoPropellerPlane() {
        this.setType();
        this.setVolume();
    }

    public void setType() {
        this.setPropulsion(this.TYPE);
    }

    public void setVolume() {
        this.setTankVolume(this.TANK_VOLUME);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "CargoPropellerPlane{} " + super.toString();
    }
}
