package droidcarrier.models;

import droidcarrier.models.Droid;

public class HelicopterDroid extends Droid {


    public HelicopterDroid(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "HelicopterDroid {" + name+
                "} ";
    }

}
