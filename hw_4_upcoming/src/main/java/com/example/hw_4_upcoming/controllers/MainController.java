package com.example.hw_4_upcoming.controllers;

import com.example.hw_4_upcoming.dao.UserDAO;
import com.example.hw_4_upcoming.models.Client;
import com.example.hw_4_upcoming.models.Restaurant;
import com.example.hw_4_upcoming.models.User;
import com.example.hw_4_upcoming.services.ContactService;
import com.example.hw_4_upcoming.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainController {
    private final UserDAO userDAO;
    private final ContactService contactService;
    private final UserServiceImpl userServiceImpl;
    private String name = "";
    private String className = "";
    private String username = "";

    @Autowired
    public MainController(UserDAO userDAO, ContactService contactService, UserServiceImpl userServiceImpl) {
        this.userDAO = userDAO;
        this.contactService = contactService;
        this.userServiceImpl = userServiceImpl;
    }

    @PostMapping("/successURL")
    public String successURL(Model model){

        Authentication auth =
                SecurityContextHolder.getContext().getAuthentication();

        User userLogged = userDAO.findByUsername(auth.getName());
        Restaurant restaurant = new Restaurant();
        Client client = new Client();
        System.out.println("userLogged.toString(): "+userLogged.toString());
        System.out.println("userLogged.getClass(): "+userLogged.getClass());

        if(userLogged.getClass().equals(com.example.hw_4_upcoming.models.Restaurant.class)){
            System.out.println("USER -> RESTAURANT");
            restaurant = (Restaurant) userLogged;
            className = "RESTAURANT";
            username = restaurant.getUsername();
            name = restaurant.getRestaurantName();
        }else if (userLogged.getClass().equals(com.example.hw_4_upcoming.models.Client.class)){
            System.out.println("USER -> CLIENT");
            client = (Client) userLogged;
            className = "CLIENT";
            username = client.getUsername();
            name = client.getClientName();
        }else {
            System.out.println("UNDEFINED CLASS!");
        }
        model.addAttribute("className", className);
        model.addAttribute("username", username);
        model.addAttribute("name", name);

        return "securedPage";
    }
}
