package handleslists;

import java.util.*;

class Utils {

    static int calculateSumReduceApproach(List<Integer> list) {
        return list.stream()
                .reduce(0, Integer::sum);
    }

    static int calculateSumMapApproach(List<Integer> list){
        return list.stream().mapToInt(Integer::intValue).sum();
    }

    static Integer calculateMin(List<Integer> list) {
        Optional<Integer> o = list.stream().min(Integer::compare);
        if (o.isPresent()){
            return o.get();
        }else {
            throw new NoSuchElementException("Element is not present");
        }
    }

    static Integer calculateMax(List<Integer> list) {
        Optional<Integer> o = list.stream().max(Integer::compare);
        if (o.isPresent()){
            return o.get();
        }else {
            throw new NoSuchElementException("Element is not present");
        }
    }

    static int calculateMaxComparator(List<Integer> list){

        Optional<Integer> o = list.stream().max(Comparator.comparing(i -> i));
        if (o.isPresent()){
            return o.get();
        }else {
            throw new NoSuchElementException("Element is not present");
        }
    }

    static float calculateAveragePlainApproach(List<Integer> list) {

        return (float) calculateSumReduceApproach(list) / list.size();
    }

    static double calculateAverageStreamApproach(List<Integer> list){

        OptionalDouble o =list.stream().mapToInt(i -> i).average();
        if (o.isPresent()){
            return o.getAsDouble();
        }else {
            throw new NoSuchElementException("Element is not present");
        }
    }


    static long calculateBiggerThanAverage(List<Integer> list) {
        float average = calculateAveragePlainApproach(list);
        return list.stream()
                .filter(value -> value > average)
                .count();
    }

    static IntSummaryStatistics calculateStatistics(List<Integer> list){

        return list.stream().mapToInt((x) -> x).summaryStatistics();
    }

}
