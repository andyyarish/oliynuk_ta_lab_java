package garden;

import garden.gardenCustomizedException.ColorException;
import garden.gardenCustomizedException.TypeException;
import garden.models.Plant;

import java.util.ArrayList;
import java.util.List;

public class PlantMainClass {

    private static Plant gardener(int size, String color, String type){

        Plant plant = new Plant();
        try {
            plant= new Plant(size, color, type);
        } catch (ColorException | TypeException e) {
            // get the cause of an ancestral exception-
            System.out.println("THE CAUSE of EXCEPTION is- "+e.getCause().toString());
            e.printStackTrace();
        }
        return plant;
    }

    public static void main(String[] args) {

        List<Plant> plants = new ArrayList<>();

        //only plants with proper arguments are created
        plants.add(gardener(1, "YELLOW", "CONIFEROUS"));
        plants.add(gardener(2, "RED", "asd"));
        plants.add(gardener(3, "qwe", "DECIDUOUS"));
        plants.add(gardener(4, "WHITE", "MOSS"));

        for (Plant plant : plants) {
            System.out.println(plant.toString());
        }

    }
}
