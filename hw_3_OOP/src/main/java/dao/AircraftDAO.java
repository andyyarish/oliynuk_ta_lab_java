package dao;

import models.Airline;
import models.abstractmodels.Aircraft;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class AircraftDAO implements AbstractDAO{

    @Override
    @SuppressWarnings("unchecked")
    public void saveAll(List<?> aircrafts) throws Exception {

        JSONArray ja = new JSONArray();
        aircrafts.forEach(a-> ja.add(gson.toJson(a)));
        FileWriter file = new FileWriter(PATH_AIRCRAFT_FILE);
        file.write(ja.toJSONString());
        file.flush();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<?> getAll() throws IOException, ParseException {

        JSONParser jsonParser = new JSONParser();
        FileReader reader = new FileReader(PATH_AIRCRAFT_FILE);
        JSONArray ja = (JSONArray) jsonParser.parse(reader);
        List<Aircraft> aircrafts = new ArrayList<>();
        ja.forEach(jsonObject-> aircrafts.add(gson.fromJson(jsonObject.toString(), Aircraft.class)));

        return aircrafts;
    }

    @SuppressWarnings("unchecked")
    public List<Aircraft> getAll(Airline airline) throws IOException, ParseException {

        List<Aircraft> aircrafts = (List<Aircraft>) this.getAll();

        return aircrafts.stream()
                .filter(a -> a.getAirline().equals(airline))
                .collect(Collectors.toList());
    }


}
