package theory.hashequals;

public class MainClassHashEquals {

    public static void main(String[] args) {

        System.out.println("Create instances of to compare");
        ModelAllFields modelAllFields1 =
                new ModelAllFields(100, "ASD", "QWE");

        ModelAllFields modelAllFields2=
                new ModelAllFields(100, "ASD", "QWE");

        ModelAllFields modelAllFields3=
                new ModelAllFields(100, "ASD", "NOT QWE");

        System.out.println("Compare instances with all fields included in hashcode and equals");

        System.out.println(modelAllFields1.equals(modelAllFields2));

        System.out.println(modelAllFields1.hashCode() == modelAllFields2.hashCode());

        System.out.println(modelAllFields1.equals(modelAllFields3));

        System.out.println(modelAllFields1.hashCode() == modelAllFields3.hashCode());

        ModelNotAllFields modelNotAllFields1 =
                new ModelNotAllFields(100, "ASD", "QWE");

        ModelNotAllFields modelNotAllFields2 =
                new ModelNotAllFields(100, "ASD", "QWE");

        ModelNotAllFields modelNotAllFields3 =
                new ModelNotAllFields(100, "ASD", "NOT QWE");

        System.out.println("Compare instances with NOT all fields included in hashcode and equals");

        System.out.println(modelNotAllFields1.equals(modelNotAllFields2));

        System.out.println(modelNotAllFields1.hashCode() == modelNotAllFields2.hashCode());

        System.out.println(modelNotAllFields1.equals(modelNotAllFields3));

        System.out.println(modelNotAllFields1.hashCode() == modelNotAllFields3.hashCode());

        System.out.println("Compare hashcodes of separate classes");

        System.out.println(modelAllFields1.hashCode() == modelNotAllFields1.hashCode());
    }
}
