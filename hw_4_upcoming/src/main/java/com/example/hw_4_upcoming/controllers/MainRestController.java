package com.example.hw_4_upcoming.controllers;


import com.example.hw_4_upcoming.dao.UserDAO;
import com.example.hw_4_upcoming.models.Client;
import com.example.hw_4_upcoming.models.Contact;
import com.example.hw_4_upcoming.models.Restaurant;
import com.example.hw_4_upcoming.models.User;
import com.example.hw_4_upcoming.services.ContactService;
//import com.sun.xml.internal.messaging.saaj.packaging.mime.MessagingException;

import com.example.hw_4_upcoming.services.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.nio.file.Path;
import java.nio.file.FileSystems;
import java.nio.file.Files;



@RestController
public class MainRestController {

    private final UserDAO userDAO;

    private final ContactService contactService;

    private final UserServiceImpl userService;

    @Autowired
    public MainRestController(UserDAO userDAO, ContactService contactService, UserServiceImpl userService) {
        this.userDAO = userDAO;
        this.contactService = contactService;
        this.userService = userService;
    }

    @PostMapping("/saveContact")
    public
    @ResponseBody
        // allows to not to return template
    User upload(
            @RequestParam("className") String className,
            @RequestParam("username") String username,
            @RequestParam("contactName") String contactName,
            @RequestParam("email") String email,
            @RequestParam("image") MultipartFile image
    ) throws Exception {

        Contact contact = new Contact(contactName, email);
        contact.setAvatar(image.getOriginalFilename());
        String path = System.getProperty("user.home")
                + File.separator
                +"images"
                +File.separator
                +image.getOriginalFilename();
        try {
            image.transferTo(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        User user = new User();
        Restaurant restaurant;
        Client client;

        if(className.equals("CLIENT")){
            client = (Client) userDAO.findByUsername(username);
            contact.setClient(client);
            contactService.save(contact);
            user= client;
        }else if (className.equals("RESTAURANT")){
            restaurant = (Restaurant) userDAO.findByUsername(username);
            contact.setRestaurant(restaurant);
            contactService.save(contact);
            user= restaurant;
        }
        return user;
    }


    @PostMapping("/showContact")
    public List<Contact> showContact(@RequestParam("username") String username){
        System.out.println("username.toString(): "+username.toString());

        List<Contact> results = new ArrayList<>();
        User userChosen = new User();

        List<User> users = userDAO.findAll();
        for (User user : users) {
            if(user.getUsername().equals(username)){
                userChosen = user;
                System.out.println(userChosen.toString());
            }
        }
        if(userChosen.getClass().equals(com.example.hw_4_upcoming.models.Client.class)){
            Client client = (Client) userChosen;
            System.out.println(client.toString());
            results = client.getClientContacts();
        }else if (userChosen.getClass().equals(com.example.hw_4_upcoming.models.Restaurant.class)){
            Restaurant restaurant = (Restaurant) userChosen;
            System.out.println(restaurant.toString());
            results = restaurant.getRestaurantContacts();
        }

        for (Contact result : results) {
            System.out.println(result.toString());
        }

        return results;
    }

    @PostMapping("/deleteAccount")
    public String deleteUser(@RequestBody User user){

        User userChosen = (User) userService.loadUserByUsername(user.getUsername());
        String response = "Account deleted";
        List<Contact> results = new ArrayList<>();
        if(userChosen.getClass().equals(com.example.hw_4_upcoming.models.Client.class)){
            Client client = (Client) userChosen;

            results = client.getClientContacts();
        }else if (userChosen.getClass().equals(com.example.hw_4_upcoming.models.Restaurant.class)){
            Restaurant restaurant = (Restaurant) userChosen;

            results = restaurant.getRestaurantContacts();
        }

        String path =
                System.getProperty("user.home") + File.separator +
                        "images" + File.separator;

        for (Contact result : results) {
            try {
                Files.delete(FileSystems.getDefault().getPath(path + result.getAvatar()));
            } catch (IOException e) {
               // response = "ERROR deleting files";
                e.printStackTrace();
            }
        }
        contactService.deleteAll(results);
        userDAO.delete(userChosen);

        return response;
    }

}
