package arrays.game.models;

public class Monster extends RandomGenerator {

    private static final int MIN_POWER = 5;
    private static final int MAX_POWER = 100;

    public int power;

    Monster() {
        power = rand.nextInt((MAX_POWER - MIN_POWER) + 1) + MIN_POWER;
    }

    @Override
    public String toString() {
        return "Monster{" +
                "power=" + power +
                '}';
    }
}
