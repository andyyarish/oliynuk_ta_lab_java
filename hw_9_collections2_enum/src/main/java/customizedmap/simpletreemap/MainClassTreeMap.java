package customizedmap.simpletreemap;

import customizedmap.Model;
import logers.BaseLoggerClass;

import java.util.Arrays;
import java.util.List;

public class MainClassTreeMap extends BaseLoggerClass {

    public static void main(String[] args) {

        generateLog(MainClassTreeMap.class.toString());

        logger.debug("Create a new CustomizedLinkedMap<Integer, String>");
        CustomizedTreeMap<Integer, String> map = new CustomizedTreeMap<>();
        logger.info("Check isEmpty(): "+map.isEmpty());
        logger.info("check size(): "+map.size());
        logger.info("Put some keys & values to an empty map");
        logger.warn(map.put(44, "ASD"));
        logger.warn(map.put(22, "QWE"));
        logger.warn(map.put(11, "CVB"));
        logger.warn(map.put(33, "HJK"));
        logger.warn("Print all the map: "+map.print());
        logger.info("Check get(33): "+map.get(33));
        logger.info("Check nonexistent get(5): "+map.get(5));
        logger.info("Check isEmpty(): "+map.isEmpty());
        logger.info("check size(): "+map.size());
        logger.info("Put a value to an existing key: "+map.put(22, "Something else"));
        logger.info("Get a new value: "+map.get(2));
        logger.info("Check size(): "+map.size());
        logger.info("Check remove: "+map.remove(33));
        logger.warn("Print all the map: "+map.print());
        logger.info("Clear all data");
        map.clear();
        logger.info("Check size(): "+map.size());
        logger.trace("-------------------------------------");
        logger.debug("Create a new CustomizedTreeMap<String, List<Integer>>");
        CustomizedTreeMap<String, List<Integer>> map1 = new CustomizedTreeMap<>();
        logger.info("Create a few lists");
        List<Integer> list1 = Arrays.asList(1, 2, 3);
        List<Integer> list2 = Arrays.asList(4, 5, 6);
        logger.warn("Put lists to an empty map: "+map1.put("A", list1));
        logger.warn("Put lists to an empty map: "+map1.put("B", list2));
        logger.warn("Print all the map: "+map1.print());
        map1.clear();
        logger.warn("Print all the map after clear() : "+map1.print());
        logger.trace("-------------------------------------");
        logger.debug("Create a new CustomizedTreeMap<Integer, List<Integer>>");
        CustomizedTreeMap<Integer, List<Model>> map2=
                new CustomizedTreeMap<>();

        logger.info("Create a few lists");
        List<Model> models1 = Arrays.asList(
                new Model(7777, "Petro"),
                new Model(2222, "Ivan"));
        List<Model> models2 = Arrays.asList(
                new Model(333, "Maria"),
                new Model(987, "Galia"));
        logger.warn("Put lists to an empty map: "+map2.put(44, models1));
        logger.warn("Put lists to an empty map: "+map2.put(66, models2));
        logger.info("Print all the map: "+map2.print());
        logger.info("Check remove(): "+map2.remove(66));
        logger.info("Check get after remove(): "+map2.get(66));
        logger.info("Get the first model instance from key 44 in the map");
        Model model = map2.get(44).get(1);
        logger.info("Print model: "+model.toString());

    }

}
