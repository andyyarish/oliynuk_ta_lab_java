package logers;

import javax.swing.*;
import java.awt.*;

public class Reader {

    private int answer;
    private String option;

    public int getAnswer() {
        return answer;
    }

    public String getOption() {
        return option;
    }

    public void request(){

        UIManager.put("Panel.background", Color.cyan);
        answer = JOptionPane.showConfirmDialog (
                null, "Do you want to test Command Interface?");
    }

    public void showDialogAllOptions(String request){

        UIManager.put("OptionPane.messageForeground", Color.blue);
        UIManager.put("Panel.background", Color.pink);
        option = JOptionPane.showInputDialog(request);
    }

    public void showDialogSingleCommand(String request){

        UIManager.put("OptionPane.messageForeground", Color.DARK_GRAY);
        UIManager.put("Panel.background", Color.yellow);
        option = JOptionPane.showInputDialog(request);
    }

    public void replyGreen(String reply){

        UIManager.put("OptionPane.messageForeground", Color.DARK_GRAY);
        UIManager.put("Panel.background", Color.green);
        JOptionPane.showMessageDialog(null, reply);
    }

    public void replyYellow(){

        UIManager.put("OptionPane.messageForeground", Color.YELLOW);
        UIManager.put("Panel.background", Color.RED);
        JOptionPane.showMessageDialog(
                null, "Wrong Input, numbers from 1 to 5 only!");
    }

    public void replyPink(String reply){

        UIManager.put("OptionPane.messageForeground", Color.BLACK);
        UIManager.put("Panel.background", Color.PINK);
        JOptionPane.showMessageDialog(null, reply);
    }
}

