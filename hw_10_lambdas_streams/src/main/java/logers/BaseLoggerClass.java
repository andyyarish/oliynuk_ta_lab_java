package logers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class BaseLoggerClass {

     protected static Logger logger =
            LogManager.getLogger(BaseLoggerClass.class);

    protected static void generateLog(String s){

        logger.warn("=> START LOGGING from: "+s+" <=");
    }

}
