package patterncommand.interfaces;

@FunctionalInterface
public interface Showable {

    void display();
}
