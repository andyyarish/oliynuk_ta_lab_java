package models.propulsiontype;

public interface Propellerable extends InternalCombustable {

    String TYPE = "Propeller Engine";
    int TANK_VOLUME= 3000;

}
