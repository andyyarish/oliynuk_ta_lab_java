package customizedpriorityqueue;

import java.util.Arrays;
import org.apache.commons.lang3.ArrayUtils;

@SuppressWarnings("unchecked")
public class CustomizedPriorityQueue<E extends Object & Comparable<E>> {

    private E[] array;

    CustomizedPriorityQueue() {
        array = (E[]) new Object[0];
    }

    boolean add(E item) {
        try {
            if (array.length == 0) {
                array = (E[]) new Object[1];
                array[0] = item;
            } else {
                E[] newItems = (E[]) new Object[array.length + 1];
                newItems[0] = item;
                for (int i = 1; i < newItems.length; i++) {
                    newItems[i] = array[i - 1];
                }
                Arrays.sort(newItems);
                array = newItems;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private E getFirst() {
        try {
            return array[0];
        } catch (Exception e) {
            throw new IndexOutOfBoundsException();
        }
    }

    // returns first element in priority
    public E peek() {
        try {
            return getFirst();
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    // removes first element in priority
    public E poll() {
        try {
            E object = array[0];
            E[] newItems = (E[]) new Object[array.length - 1];
            for (int i = 0; i < newItems.length; i++) {
                newItems[i] = array[i + 1];
            }
            array = newItems;
            return object;
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public boolean contains(E object) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(object)) {
                return true;
            }
        }
        return false;
    }


    public boolean remove(E object) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(object)) {
                array = ArrayUtils.removeElement(array, array[i]);
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "CustomizedPriorityQueue " + Arrays.toString(array);
    }

}
