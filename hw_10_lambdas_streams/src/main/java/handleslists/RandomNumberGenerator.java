package handleslists;

//https://github.com/DiUS/java-faker/blob/master/src/main/java/com/github/javafaker/Number.java
import com.github.javafaker.Faker;
import java.math.BigDecimal;


class RandomNumberGenerator {

    private final Faker faker;

    RandomNumberGenerator() {
        faker = new Faker();
    }

    int numberBetween(int min, int max) {
        if (min == max) return min;

        int value = decimalBetween(min,max).setScale(0,
                BigDecimal.ROUND_HALF_DOWN).intValue();
        return value == max ? value - 1 : value;
    }

    private BigDecimal decimalBetween(long min, long max) {
        if (min == max) {
            return new BigDecimal(min);
        }
        final long trueMin = Math.min(min, max);
        final long trueMax = Math.max(min, max);

        final double range = (double) trueMax - (double) trueMin;

        final double chunkCount = Math.sqrt(Math.abs(range));
        final long randomChunk = faker.random().nextLong((long) chunkCount);

        final double chunkStart = trueMin + randomChunk * chunkCount;
        final double adj = chunkCount * faker.random().nextDouble();
        return new BigDecimal(chunkStart + adj);
    }

}
