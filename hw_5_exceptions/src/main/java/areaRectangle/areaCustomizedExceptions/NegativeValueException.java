package areaRectangle.areaCustomizedExceptions;

public class NegativeValueException extends Exception {
    public NegativeValueException(String message) {
        super(message);
    }
}
