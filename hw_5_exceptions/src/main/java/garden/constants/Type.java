package garden.constants;

public enum Type {

    CONIFEROUS,
    DECIDUOUS,
    FRUIT,
    MOSS
}
