package ui;

import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

    private static Scanner scanner = new Scanner(System.in);

    private static Logger logger = LogManager.getLogger(App.class);

    private static String reply = "\nChose the following demo items:" +
                "\n1.printAllAircraftsSortedByAirline" +
                "\n2.printAircraftsOfAirline" +
                "\n3.saveNewAirline" +
                "\n4.deleteExistingAirline" +
                "\n5.addNewAircraft" +
                "\n6.getCargoAircrafts" +
                "\n7.getHelicopters" +
                "\n8.deleteAircraft" +
                "\n9.getTotalPassengerCapacity" +
                "\n10.getTotalLoadCapacity" +
                "\n11.sortByFlightRange" +
                "\n12.findByFuelConsumption" +
                "\n13.getSimilarAircraftsFromDB" +
                "\n14.Quit";


    private static int getChoice(){

        logger.info(reply);

        int choiceFromScanner = 0;
        String line = scanner.nextLine();
            try {
                choiceFromScanner = Integer.parseInt(line);

            } catch (NumberFormatException e) {
                logger.error("Ahtung!! Numbers only!");
            }
        return choiceFromScanner;
    }

    public static void run() throws Exception {

        DemoCases demo = new DemoCases();

        int choice = getChoice();

        while (!(choice == 14)) {
            switch (choice) {
                case 1: {
                    demo.printAllAircraftsSortedByAirline();
                    choice= getChoice();
                    break;
                }
                case 2: {
                    demo.printAircraftsOfAirline();
                    choice= getChoice();
                    break;
                }
                case 3: {
                    demo.saveNewAirline();
                    choice = getChoice();
                    break;
                }
                case 4: {
                    demo.deleteExistingAirline();
                    choice = getChoice();
                    break;
                }
                case 5: {
                    demo.addNewAircraft();
                    choice = getChoice();
                    break;
                }
                case 6: {
                    demo.getCargoAircrafts();
                    choice = getChoice();
                    break;
                }
                case 7: {
                    demo.getHelicopters();
                    choice = getChoice();
                    break;
                }
                case 8: {
                    demo.deleteAircraft();
                    choice = getChoice();
                    break;
                }
                case 9: {
                    demo.getTotalPassengerCapacity();
                    choice = getChoice();
                    break;
                }
                case 10: {
                    demo.getTotalLoadCapacity();
                    choice = getChoice();
                    break;
                }
                case 11: {
                    demo.sortByFlightRange();
                    choice = getChoice();
                    break;
                }
                case 12: {
                    demo.findByFuelConsumption();
                    choice = getChoice();
                    break;
                }
                case 13: {
                    demo.getSimilarAircraftsFromDB();
                    choice = getChoice();
                    break;
                }
                default: {
                    logger.debug("Wrong input, from 1 to 14 only!");
                    choice = getChoice();
                    break;
                }
            }

        }

    }
}
