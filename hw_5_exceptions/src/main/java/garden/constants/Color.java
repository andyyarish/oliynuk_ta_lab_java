package garden.constants;

public enum Color {
    RED,
    WHITE,
    BLACK,
    YELLOW
}
