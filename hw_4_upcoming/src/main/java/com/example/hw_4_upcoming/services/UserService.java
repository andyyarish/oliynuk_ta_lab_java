package com.example.hw_4_upcoming.services;

import com.example.hw_4_upcoming.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    void save(User user);

    void delete(User user);
   // User findByUsername(String username);

    
    List<User> findAll();

    User findOneById(Integer id);

}