package arrays.game.models;

public class Door extends RandomGenerator {

    public boolean isMonster;
    public Monster monster;
    public Artifact artifact;

    public Door() {
        isMonster = rand.nextBoolean();
        if (isMonster) {
            monster = new Monster();
        } else {
            artifact = new Artifact();
        }
    }

    @Override
    public String toString() {
        return "Door{" +
                "isMonster=" + isMonster +
                ", monster=" + monster +
                ", artifact=" + artifact +
                "} " ;
    }
}
